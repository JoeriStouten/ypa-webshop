import logging
import json
import pickle
import uuid
from datetime import datetime
from flask import Flask, render_template, flash, request, redirect

app = Flask(__name__)
json_file = 'products.json'

def json_to_pickle():
    json_file = 'products.json'

    with open(json_file) as f:
        d = json.load(f)
        products = d['products']
        for product in products:
            product.update({'uuid': str(uuid.uuid4().hex)})
        with open('metapickle.pickle', 'wb') as file:
            pickle.dump(products, file, protocol=pickle.HIGHEST_PROTOCOL)


@app.route('/')
def index():
    with open('metapickle.pickle', 'rb') as file:
        meta_dump = pickle.load(file)

    category_dict = {}

    for product in meta_dump:
        category_dict[product['category']] = ''

    category_dict['electronics'] = 'https://cdn.shopify.com/s/files/1/0070/7032/files/stock-photography.jpg?v=1554609552&width=1024'
    category_dict['toys'] = 'https://www.oxygenna.com/wp-content/uploads/2015/11/18.jpg'
    category_dict['books'] = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSN680XkohHNBqD99WjTRI5ulWGZJU7KpWgzg&usqp=CAU'
    category_dict['clothing'] = 'https://www.itl.cat/pngfile/big/212-2125652_background-image-material-design.jpg'
    category_dict['kitchen'] = 'https://cdn.wallpapersafari.com/65/1/atQZcY.jpg'
    category_dict['home'] = 'https://undesigns.net/wp-content/uploads/2018/02/Material-Design-Background-Undesigns-00.jpg'

    return render_template('index.html', meta_data=meta_dump, category_dict=category_dict)


@app.route('/images/<category>')
def images(category):
    image_list = []

    with open('metapickle.pickle', 'rb') as file:
        meta_dump = pickle.load(file)

    for data in meta_dump:
        if category in data['category']:
            image_list.append(data)

    return render_template('images.html', images=image_list, meta_data=meta_dump)


@app.route('/details/<product_id>')
def details(product_id):

    with open('metapickle.pickle', 'rb') as file:
        meta_dump = pickle.load(file)

    for data in meta_dump:
        if product_id in data['uuid']:
            prod_ = data
    return render_template('details.html', meta_data=prod_)


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080, debug=True)